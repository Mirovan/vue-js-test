import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Index from '@/components/Index.vue'
import About from '@/components/About.vue'
import Contact from '@/components/Contact.vue'

Vue.use(VueRouter)

const routes = [
  {path: '/', component: Index},
  {path: '/about/', component: About},
  {path: '/contact/', component: Contact}
]

const router = new VueRouter({
  mode: 'history',
  routes: routes
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
